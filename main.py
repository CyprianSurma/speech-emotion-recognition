from os import listdir
from os.path import isfile, join
import numpy as numpy
from scipy.fftpack import dct
from sklearn import preprocessing
from sklearn import svm
from tabulate import tabulate

def getAllFilesFromDir(dir):   
    return  [f for f in listdir(dir) if isfile(join(dir, f))]

def getWavFile(file):
    from scipy.io import wavfile
    return wavfile.read(file)

def getArrayForFile(name):
    sample_rate, data = getWavFile('wav/'+name)
    if len(data) != 143652:
        numpy.append(data,numpy.zeros(143652- len(data)))
    #print (sample_rate,data)
    data = data[0:19608]
    #pre
    pre_emphasis = 0.97
    emphasized_signal = numpy.append(data[0], data[1:] - pre_emphasis * data[:-1])
    #splitting
    frame_size = 0.05
    frame_stride = 0.01
    frame_length, frame_step = frame_size * sample_rate, frame_stride * sample_rate  # Convert from seconds to samples
    signal_length = len(emphasized_signal)
    frame_length = int(round(frame_length))
    frame_step = int(round(frame_step))
    num_frames = int(numpy.ceil(float(numpy.abs(signal_length - frame_length)) / frame_step))  # Make sure that we have at least 1 frame

    pad_signal_length = num_frames * frame_step + frame_length
    z = numpy.zeros((pad_signal_length - signal_length))
    pad_signal = numpy.append(emphasized_signal, z) # Pad Signal to make sure that all frames have equal number of samples without truncating any samples from the original signal

    indices = numpy.tile(numpy.arange(0, frame_length), (num_frames, 1)) + numpy.tile(numpy.arange(0, num_frames * frame_step, frame_step), (frame_length, 1)).T
    frames = pad_signal[indices.astype(numpy.int32, copy=False)]

    #windowing
    frames *= numpy.hamming(frame_length)
    #fft
    NFFT = 512
    mag_frames = numpy.absolute(numpy.fft.rfft(frames, NFFT))  # Magnitude of the FFT
    pow_frames = ((1.0 / NFFT) * ((mag_frames) ** 2))  # Power Spectrum

    nfilt = 40
    low_freq_mel = 0
    high_freq_mel = (2595 * numpy.log10(1 + (sample_rate / 2) / 700))  # Convert Hz to Mel
    mel_points = numpy.linspace(low_freq_mel, high_freq_mel, nfilt + 2)  # Equally spaced in Mel scale
    hz_points = (700 * (10**(mel_points / 2595) - 1))  # Convert Mel to Hz
    bin = numpy.floor((NFFT + 1) * hz_points / sample_rate)

    fbank = numpy.zeros((nfilt, int(numpy.floor(NFFT / 2 + 1))))
    for m in range(1, nfilt + 1):
        f_m_minus = int(bin[m - 1])   
        f_m = int(bin[m])             
        f_m_plus = int(bin[m + 1])    

        for k in range(f_m_minus, f_m):
            fbank[m - 1, k] = (k - bin[m - 1]) / (bin[m] - bin[m - 1])
        for k in range(f_m, f_m_plus):
            fbank[m - 1, k] = (bin[m + 1] - k) / (bin[m + 1] - bin[m])
    
    filter_banks = numpy.dot(pow_frames, fbank.T)
    filter_banks = numpy.where(filter_banks == 0, numpy.finfo(float).eps, filter_banks)  
    m = numpy.log10(filter_banks)
    filter_banks = 20 * numpy.log10(filter_banks)  # dB
    num_ceps = 12
    mean = numpy.array([])
    for i in m:
        mean = numpy.append(mean,i.mean())

    first = numpy.diff(mean)
    second =  numpy.diff(first)
    mfcc = dct(filter_banks, type=2, axis=1, norm='ortho')[:, 1 : (num_ceps + 1)] 
   
    return numpy.append( mfcc.flatten(),first[1:len(first)] - second),name[5]

def isMale(name):
    maleValues = ["03", "10", "11", "12","15"]
    if name[0:2] in maleValues:
        return True
    return False    
def printResults(table):    
    print(tabulate(table, tablefmt="latex", floatfmt=".2f", headers = ["neutral",  "anger", "boredom", "disgust", "anxiety", "happines", "saddnes"]))

def onlyManScenario(kernel):
    wavFIles = getAllFilesFromDir("wav")
    k = 1.5
    p= []
    X = []
    for i in wavFIles:
        if(isMale(i)):
            a,b = getArrayForFile(i)
            p.append(b)
            X.append(a)
    learnIndex = len(X)-90
    learn = X[:learnIndex]
    test = X[learnIndex:]
    le = preprocessing.LabelEncoder()
    le.fit(['N', 'W' ,'L' ,'E' ,'A', 'F', 'T'])#neutral  anger boredom disgust anxiety happines saddnes
    myset = set(p)
    labels = le.transform(p)
    learnLabels = labels[:learnIndex]
    testLabels = labels[learnIndex:]
    clf = svm.SVC(kernel = kernel)
    clf.fit(learn, learnLabels)

    x = 0
    emotions = numpy.zeros((7,7))
    result = clf.predict(X)
    for i in range(0,len(X)):
        emotions[labels[i]][result[i]] +=1
        if labels[i] == result[i]:
            x+=1
    for i in emotions:
        i /=sum(i)
        i *= 100
    print("only man kernel " + kernel)
    printResults(emotions)

def onlyWomanScenario(kernel):
    wavFIles = getAllFilesFromDir("wav")
    k = 1.5
    p= []
    X = []
    for i in wavFIles:
        if(not isMale(i)):
            a,b = getArrayForFile(i)
            p.append(b)
            X.append(a)
    learnIndex = len(X)-90
    learn = X[:learnIndex]
    test = X[learnIndex:]
    le = preprocessing.LabelEncoder()
    le.fit(['N', 'W' ,'L' ,'E' ,'A', 'F', 'T'])#neutral  anger boredom disgust anxiety happines saddnes
    myset = set(p)
    labels = le.transform(p)
    learnLabels = labels[:learnIndex]
    testLabels = labels[learnIndex:]
    clf = svm.SVC(kernel = kernel)
    clf.fit(learn, learnLabels)

    x = 0
    emotions = numpy.zeros((7,7))
    result = clf.predict(X)
    for i in range(0,len(X)):
        emotions[labels[i]][result[i]] +=1
        if labels[i] == result[i]:
            x+=1
    for i in emotions:
        i /=sum(i)
        i *= 100
    print("only Woman kernel " + kernel)
    printResults(emotions)

def allScenario(kernel):
    wavFIles = getAllFilesFromDir("wav")
    k = 1.5
    p= []
    X = []
    for i in wavFIles:
        a,b = getArrayForFile(i)
        p.append(b)
        X.append(a)
    learnIndex = len(X)-90
    learn = X[:learnIndex]
    test = X[learnIndex:]
    le = preprocessing.LabelEncoder()
    le.fit(['N', 'W' ,'L' ,'E' ,'A', 'F', 'T'])#neutral  anger boredom disgust anxiety happines saddnes
    myset = set(p)
    labels = le.transform(p)
    learnLabels = labels[:learnIndex]
    testLabels = labels[learnIndex:]
    clf = svm.SVC(kernel = kernel)
    clf.fit(learn, learnLabels)

    x = 0
    emotions = numpy.zeros((7,7))
    result = clf.predict(X)
    for i in range(0,len(X)):
        emotions[labels[i]][result[i]] +=1
        if labels[i] == result[i]:
            x+=1
    for i in emotions:
        i /=sum(i)
        i *= 100
    print("mixed kernel " + kernel)
    printResults(emotions)    

onlyManScenario('rbf')
onlyWomanScenario('rbf')
allScenario('rbf')

onlyManScenario('poly')
onlyWomanScenario('poly')
allScenario('poly')